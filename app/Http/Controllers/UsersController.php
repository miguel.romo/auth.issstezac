<?php

namespace App\Http\Controllers;

Use App\Models\Usuario;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UsersTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreUserRequest;



class UsersController extends Controller
{ 

    use Helpers;


    /**
     * Show all Users.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = Usuario::all();
        
        return $this->collection($users, new UsersTransformer);
    }


    /**
     * Show data of the specified User.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = Usuario::where('id', $id)->first();

        if ($user) {
            return $this->item($user, new UsersTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new User.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreUserRequest $request)
    {

        $id = Usuario::create($request->all());
        if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the User.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreUserRequest $request, $id)
    {

            $user = Usuario::findOrFail($id);
    
            $user->update($request->all());

    
        if ($user) {
            return $this->item($user, new UsersTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified User.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Usuario::find($id);

        if ($user) {
            $user->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }


    /**
     * Validate Input Values of the Request
     *
     * @param  Request $request    
     *
     * @return \Illuminate\Http\Response
     */
    private function validateRequest(Request $request)
    {
            
           $validator = Validator::make($request->all(), [
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',  
                'password_confirmation' => 'required|min:6',       
                'nombre'      => 'required|string|max:50',
                'primerApellido' => 'required|string|max:50',
                'segundoApellido'=> 'required|string|max:50',
                'iniciales'      => 'required|string|max:20',
                'idRol'          => 'required|numeric',

            ]);

            if($validator->fails()){
                    return response()->json($validator->errors()->toJson(), 400);
            }
            
    }

}