<?php

namespace App\Http\Controllers;

Use App\Models\Domicilio;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\DomicilioTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreDomicilioRequest;



class DomicilioController extends Controller
{ 

    use Helpers;


    /**
     * Show data of the specified domicilio.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $domicilio = Domicilio::where('id', $id)->first();

        if ($domicilio) {
            return $this->item($domicilio, new DomicilioTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new Domicilio.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreDomicilioRequest $request)
    {

         $id = Domicilio::insertGetId($request->all());
        if ($id) {
                 return $id;
        }


        return $this->response->errorBadRequest();
    }

    /**
     * Update the Domicilio.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreDomicilioRequest $request, $id)
    {

            $domicilio = Domicilio::findOrFail($id);
    
            $domicilio->update($request->all());

    
        if ($domicilio) {
            return $this->item($domicilio, new DomicilioTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified domicilio.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $domicilio = Domicilio::find($id);

        if ($domicilio) {
            $domicilio->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }

   

}