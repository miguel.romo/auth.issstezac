<?php

namespace App\Http\Controllers;

Use App\Models\Asentamiento;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\AsentamientoTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class AsentamientoController extends Controller
{ 

    use Helpers;

    /**
     * Show data of the specified Municipio.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


        $asentamiento = Asentamiento::where('id', $id)->first();

        if ($asentamiento) {
            return $this->item($asentamiento, new AsentamientoTransformer);
        }

        return $this->response->errorNotFound();
    }





}