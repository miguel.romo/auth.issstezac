<?php

namespace App\Http\Controllers;

Use App\Models\EstadoCivil;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\EstadoCivilTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreEstadoCivilRequest;



class EstadoCivilController extends Controller
{ 

    use Helpers;


    /**
     * Show all estados civiles.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = EstadoCivil::all();
        
        return $this->collection($estados, new EstadoCivilTransformer);
    }


    /**
     * Show data of the specified Estado civil.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $estado = EstadoCivil::where('id', $id)->first();

        if ($estado) {
            return $this->item($estado, new EstadoCivilTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new Estado civil.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreEstadoCivilRequest $request)
    {

           $id = EstadoCivil::insertGetId($request->all());
           if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the Estado civil.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreEstadoCivilRequest $request, $id)
    {

            $estado = EstadoCivil::findOrFail($id);
    
            $estado->update($request->all());

    
        if ($estado) {
            return $this->item($estado, new EstadoCivilTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified Estado civil.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estado = EstadoCivil::find($id);

        if ($estado) {
            $estado->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }




}