<?php

namespace App\Http\Controllers;

Use App\Models\Municipio;
Use App\Models\CodigoPostal;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\CodigoPostalTransformer;
use App\Transformers\MunicipioTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class MunicipioController extends Controller
{ 

    use Helpers;

    /**
     * Show data of the specified Municipio.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $municipio = Municipio::where('id', $id)->first();

        if ($municipio) {
            return $this->item($municipio, new MunicipioTransformer);
        }

        return $this->response->errorNotFound();
    }


  


    /**
     * Regresa la lista de Codigos postales por id de municipio.
     *
     * @param  int $idEntidad
     *
     * @return \Illuminate\Http\Response
     */
    public function codigospostales($id)
    {

           $codigos = CodigoPostal::select('municipios_codigopostal.*')
                ->where('municipios_codigopostal.idMunicipio',$id)
                ->orderby('municipios_codigopostal.nombre')
                ->get();
           
        
         return $this->response->array(['data' => $codigos], 200);
    }


}