<?php

namespace App\Http\Controllers;

Use App\Models\Derechohabiente;
Use App\Models\Beneficiario;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\DerechohabienteTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreDerechohabienteRequest;



class DerechohabienteController extends Controller
{ 

    use Helpers;


    /**
     * Show all Derechohabientes.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $derechohabientes = Derechohabiente::all();
        
        return $this->collection($derechohabientes, new DerechohabienteTransformer);
    }


    /**
     * Show data of the specified Derechohabiente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $derechohabiente = Derechohabiente::where('id', $id)->first();

        if ($derechohabiente) {
            return $this->item($derechohabiente, new DerechohabienteTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new Derechohabiente.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreDerechohabienteRequest $request)
    { 

        $id = Derechohabiente::insertGetId($request->all());
        if ($id) {
                 return $id;
            }


        return $this->response->errorBadRequest();
    }

    /**
     * Update the Derechohabiente.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreDerechohabienteRequest $request, $id)
    {

            $derechohabiente = Derechohabiente::findOrFail($id);
    
            $derechohabiente->update($request->all());

    
        if ($derechohabiente) {
            return $this->item($derechohabiente, new DerechohabienteTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified Derechohabiente.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $derechohabiente = Derechohabiente::find($id);

        if ($derechohabiente) {
            $derechohabiente->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }



    /**
     * Return the Beneficarios list by Derechohabiente id.
     *
     * @param  int $idEntidad
     *
     * @return \Illuminate\Http\Response
     */
    public function beneficiarios($idDerechohabiente)
    {

           $beneficiarios = Beneficiario::select('beneficiario.id as idBeneficiario',
                'beneficiario.*')
                ->where('beneficiario.idDerechohabiente',$idDerechohabiente)
                ->orderby('beneficiario.id')
                ->get();
           
        
       return $this->response->array(['data' => $beneficiarios], 200);
    }



}