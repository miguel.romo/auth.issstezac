<?php

namespace App\Http\Controllers;

Use App\Models\CodigoPostal;
Use App\Models\Asentamiento;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\CodigoPostalTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;



class CodigoPostalController extends Controller
{ 

    use Helpers;


    /**
     * Mostrar datos del Codigo postal.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $codigopostal = CodigoPostal::where('id', $id)->first();

        if ($codigopostal) {
            return $this->item($codigopostal, new CodigoPostalTransformer);
        }

        return $this->response->errorNotFound();
    }



  /**
     * Regresa la lista de asentamientos por id del código postal
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function asentamientos($id)
    {

           $asentamientos = Asentamiento::select('municipios_asentamiento.*')
                ->where('municipios_asentamiento.idCodigoPostal',$id)
                ->orderby('municipios_asentamiento.ciudad')
                ->get();
           
        
       return $this->response->array(['data' => $asentamientos], 200);
    }


}