<?php

namespace App\Http\Controllers;

Use App\Models\Beneficiario;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\BeneficiarioTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreBeneficiarioRequest;



class BeneficiarioController extends Controller
{ 

    use Helpers;

    /**
     * Show data of the specified Beneficiario.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $beneficiario = Beneficiario::where('id', $id)->first();

        if ($beneficiario) {
            return $this->item($beneficiario, new BeneficiarioTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new Beneficiario.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreBeneficiarioRequest $request)
    {

           $id = Beneficiario::insertGetId($request->all());
           if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the Beneficiario.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreBeneficiarioRequest $request, $id)
    {

            $beneficiario = Beneficiario::findOrFail($id);
    
            $beneficiario->update($request->all());

    
        if ($beneficiario) {
            return $this->item($beneficiario, new BeneficiarioTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified beneficiario.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $beneficiario = Beneficiario::find($id);

        if ($beneficiario) {
            $beneficiario->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }







}