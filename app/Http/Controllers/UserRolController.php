<?php

namespace App\Http\Controllers;

Use App\Models\UserRol;
use Illuminate\Http\Request;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserRolTransformer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreUserRequest;



class UserRolController extends Controller
{ 

    use Helpers;


    /**
     * Show all User Roles.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = UserRol::all();
        
        return $this->collection($users, new UserRolTransformer);
    }


    /**
     * Show data of the specified User Rol.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $user = UserRol::where('id', $id)->first();

        if ($user) {
            return $this->item($user, new UserRolTransformer);
        }

        return $this->response->errorNotFound();
    }


    /**
     * Creata a new User Rol.
     *
     * @param  Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\StoreUserRolRequest $request)
    {

        $id = UserRol::insertGetId($request->all());
        if ($id) {
                 return $id;
            }

        return $this->response->errorBadRequest();
    }

    /**
     * Update the User.
     *
     * @param  Request $request
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */

    public function update(Requests\StoreUserRolRequest $request, $id)
    {

            $userRol = UserRol::findOrFail($id);
    
            $userRol->update($request->all());

    
        if ($userRol) {
            return $this->item($userRol, new UserRolTransformer);
        }
        
        return $this->response->errorBadRequest();
    }

    /**
     * Remove the specified User Rol.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userRol = UserRol::find($id);

        if ($userRol) {
            $userRol->delete();
            return $this->response->noContent();
        }

        return $this->response->errorBadRequest();
    }


}