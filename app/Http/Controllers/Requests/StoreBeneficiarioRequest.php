<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreBeneficiarioRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'estatus'   => 'required', 
                'rfc'       => 'required', 
                'curp'      => 'required', 
                'nombre'    => 'required', 
                'primerApellido'  => 'required', 
                'segundoApellido' => 'required', 
                'fechaNac'  => 'required', 
                'sexo'      => 'required',
                'telefono'  => 'required', 
                'esDerechohabiente' => 'required',  
                'idGrupoSanguineo'  => 'required', 
                'idDerechohabiente'      => 'required',
                'idParentesco'      => 'required', 
                'idEstadoCivil'     => 'required',
        ];
    }


    public function messages()
    {
        return [
                'estatus.required'   => 'Estatus requerido', 
                'rfc.required'       => 'RFC requerido', 
                'curp.required'      => 'CURP requerida', 
                'nombre.required'    => 'Nombre requerido', 
                'primerApellido.required'  => 'Primer apellido requerido', 
                'segundoApellido.required' => 'Segundo apellido requerido', 
                'fechaNac.required'  => 'Fecha de nacimiento requerida', 
                'sexo.required'      => 'Sexo requerido',
                'telefono.required'  => 'Teléfono requerido', 
                'esDerechohabiente.required' => 'Es derechohabiente requerido',  
                'idGrupoSanguineo.required'  => 'Grupo sanguíneo requerido', 
                'idDerechohabiente.required'      => 'Derechohabiente requerido',
                'idParentesco.required'      => 'Parentesco requerido', 
                'idEstadoCivil.required'     => 'Estado civil requerido',
            ];

    }

}