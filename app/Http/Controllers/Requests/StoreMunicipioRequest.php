<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreMunicipioRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'nombre' => 'required',
                'cveMun' => 'required',
                'idEntidadFederativa' => 'required',
                
    
        ];
    }


    public function messages()
    {
        return [
                'nombre.required'=> 'Nombre del municipio requerido',
                'cveMun.required'=> 'Clave del municipio requerida',
                'idEntidadFederativa.required' => 'Entidad Federativa requerida',
                
        ];

    }

}