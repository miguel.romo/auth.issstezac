<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreDerechohabienteTipoRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'descripcion' => 'required',
                'inicial' => 'required',
       
        ];
    }


    public function messages()
    {
        return [
                'descripcion.required'=> 'Descripción del Tipo de derechohabiente requerida',
                'inicial.required'=> 'Inicial del tipo de derechohabiente requerida',         
        ];

    }

}