<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreUserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'username' => 'required|string|max:255|unique:users',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',  
                'password_confirmation' => 'required|min:6',       
                'nombre'      => 'required|string|max:50',
                'primerApellido' => 'required|string|max:50',
                'segundoApellido'=> 'required|string|max:50',
                'iniciales'      => 'required|string|max:20',
                'idRol'          => 'required|numeric',
        ];
    }


    public function messages()
    {
        return [
                'username.required'=> 'Nombre de usuario requerido',
                'email.required' => 'Email requerido', 
                'password.required'=> 'Contraseña requerida',
                'password_confirmation.requierd' => 'Contraseña de confirmación requerida',
                'nombre.required'         => 'Nombre requerido',
                'primerApellido.required' => 'Primer apellido requerido',
                'segundoApellido.required'=> 'Segundo apellido requerido',
                'iniciales.required'      => 'Iniciales requeridas',
                'idRol.required'          => 'Rol requerido',

        ];

    }

}