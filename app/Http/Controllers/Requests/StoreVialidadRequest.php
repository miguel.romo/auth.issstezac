<?php

namespace App\Http\Controllers\Requests;

use Dingo\Api\Http\FormRequest;

class StoreVialidadRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'vialidad' => 'required',
        
        ];
    }


    public function messages()
    {
        return [
                'vialidad.required' => 'Descripción de vialidad requerida',
                
        ];

    }

}