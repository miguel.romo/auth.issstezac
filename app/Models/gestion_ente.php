<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idGestionEnteInt
 * @property string $nombreTitularStr
 * @property string $fechaInicioGestionDte
 * @property string $fechaFinGestionDte
 * @property int $idEnteLng
 * @property EntePublico[] $entePublicos
 */
class gestion_ente extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'gestion_ente';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idGestionEnteInt';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreTitularStr', 'fechaInicioGestionDte', 'fechaFinGestionDte', 'idEnteLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'idEnteLng', 'idGestionEnteInt');
    }
}
