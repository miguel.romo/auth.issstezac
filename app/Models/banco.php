<?php

namespace  App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idBancoLng
 * @property string $nombreStr
 * @property string $numeroBancoStr
 * @property RelacionLaboral[] $relacionLaborals
 */
class banco extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'banco';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idBancoLng';

    /**
     * @var array
     */
    protected $fillable = ['nombreStr', 'numeroBancoStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relacionLaborals()
    {
        return $this->hasMany('App\Models\relacion_laboral', 'idBancoLng', 'idBancoLng');
    }
}
