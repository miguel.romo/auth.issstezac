<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idDestinatarioMensajeLng
 * @property int $idMensajeLng
 * @property int $idDestinatarioLng
 * @property string $estatusMensajeDestinatarioStr
 * @property Mesaje[] $mesajes
 */
class destinatario_mensaje extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'destinatario_mensaje';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idDestinatarioMensajeLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idMensajeLng', 'idDestinatarioLng', 'estatusMensajeDestinatarioStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mensajes()
    {
        return $this->hasMany('App\Models\mensaje', 'destinatario_mensaje_idDestinatarioMensajeLng', 'idDestinatarioMensajeLng');
    }
}
