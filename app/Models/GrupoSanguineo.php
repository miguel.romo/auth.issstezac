<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $grupoSanguineo
 */
class GrupoSanguineo extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'grupo_sanguineo';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['grupoSanguineo'];


    public $timestamps = false;
    
}
