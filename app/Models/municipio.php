<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $codigo
 * @property int $idEstado
 * @property Estado $Estado
 * @property CodigosPostales[] $CodigoPostales
 */
class Municipio extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'municipios_municipio';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'codigo', 'idEstado'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Estado()
    {
        return $this->belongsTo('App\Model\Estado', 'idEstado');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function CodigosPostales()
    {
        return $this->hasMany('App\Models\CodigosPostales', 'idMunicipio');
    }
}
