<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idLeyLng
 * @property string $nombreLeyStr
 * @property string $descripcionStr
 * @property int $relacion_laboral_idRelacionLaboralLng
 */
class ley extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ley';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idLeyLng';

    /**
     * @var array
     */
    protected $fillable = ['nombreLeyStr', 'descripcionStr', 'relacion_laboral_idRelacionLaboralLng'];

}
