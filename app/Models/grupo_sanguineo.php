<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idGrupoSanguineoLng
 * @property string $grupoSanguineoStr
 * @property int $beneficiarios_idBeneficiarioLng
 * @property Beneficiario $beneficiario
 * @property Derechohabiente[] $derechohabientes
 */
class grupo_sanguineo extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'grupo_sanguineo';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idGrupoSanguineoLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['grupoSanguineoStr', 'beneficiarios_idBeneficiarioLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function beneficiario()
    {
        return $this->belongsTo('App\Models\beneficiarios', 'idGrupoSanguineoLng', 'idBeneficiarioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derechohabientes()
    {
        return $this->hasMany('App\Models\derechohabiente', 'idGrupoSanguineoLng', 'idGrupoSanguineoLng');
    }
}
