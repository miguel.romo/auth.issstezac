<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idTipoDerechohabienteLng
 * @property string $descripcionStr
 * @property string $inicialTipoDerechohabienteStr
 * @property Derechohabiente[] $derechohabientes
 */
class tipo_derechohabiente extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_derechohabiente';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idTipoDerechohabienteLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['descripcionStr', 'inicialTipoDerechohabienteStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derechohabientes()
    {
        return $this->hasMany('App\Models\derechohabiente', 'idTipoDerechohabienteLng', 'idTipoDerechohabienteLng');
    }
}
