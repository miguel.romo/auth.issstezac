<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idEnteLng
 * @property string $nombreEnteStr
 * @property string $rfcPatronalStr
 * @property string $telefonoStr
 * @property string $clasificacionStr
 * @property string $fechaIncorporacionDte
 * @property string $fechaDesincorporacionDte
 * @property float $porcentajeCuotaDbl
 * @property float $porcentajeAportacionDbl
 * @property string $claveRegistroPatronal
 * @property int $idRegimenIncorporacionInt
 * @property int $idEstatusEnteInt
 * @property int $representante_legal_idRepresentanteLegal
 * @property int $tabulador_idTabuladorLng
 * @property integer $domicilio_idDomicilioLng
 * @property int $calendario_cobros_idCalendarioCobrosLng
 * @property int $concepto_ente_idConceptoEnteLng
 * @property DocumentoDigitalizado $documentoDigitalizado
 * @property CalendarioCobro $calendarioCobro
 * @property ConceptoEnte $conceptoEnte
 * @property Domicilio $domicilio
 * @property EstatusEnte $estatusEnte
 * @property GestionEnte $gestionEnte
 * @property RepresentanteLegal $representanteLegal
 * @property Tabulador $tabulador
 * @property TipoRegiman $tipoRegiman
 * @property AsignacionAmbito[] $asignacionAmbitos
 * @property RelacionLaboral[] $relacionLaborals
 */
class ente_publico extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ente_publico';

    /**
     * @var array
     */
    protected $fillable = ['idEnteLng', 'nombreEnteStr', 'rfcPatronalStr', 'telefonoStr', 'clasificacionStr', 'fechaIncorporacionDte', 'fechaDesincorporacionDte', 'porcentajeCuotaDbl', 'porcentajeAportacionDbl', 'claveRegistroPatronal', 'idRegimenIncorporacionInt', 'idEstatusEnteInt', 'representante_legal_idRepresentanteLegal', 'tabulador_idTabuladorLng', 'domicilio_idDomicilioLng', 'calendario_cobros_idCalendarioCobrosLng', 'concepto_ente_idConceptoEnteLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function documentoDigitalizado()
    {
        return $this->belongsTo('App\Models\documento_digitalizado', 'idEnteLng', 'idRelacionadoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function calendarioCobro()
    {
        return $this->belongsTo('App\Models\calendario_cobros', 'calendario_cobros_idCalendarioCobrosLng', 'idCalendarioCobrosLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conceptoEnte()
    {
        return $this->belongsTo('App\Models\concepto_ente', 'concepto_ente_idConceptoEnteLng', 'idConceptoEnteLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function domicilio()
    {
        return $this->belongsTo('App\Models\domicilio', 'domicilio_idDomicilioLng', 'idDomicilioLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estatusEnte()
    {
        return $this->belongsTo('App\Models\estatus_ente', 'idEstatusEnteInt', 'idEstatusEnteInt');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gestionEnte()
    {
        return $this->belongsTo('App\Models\gestion_ente', 'idEnteLng', 'idGestionEnteInt');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function representanteLegal()
    {
        return $this->belongsTo('App\Models\representante_legal', 'representante_legal_idRepresentanteLegal', 'idRepresentanteLegal');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tabulador()
    {
        return $this->belongsTo('App\Models\tabulador', 'tabulador_idTabuladorLng', 'idTabuladorLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoRegiman()
    {
        return $this->belongsTo('App\Models\tipo_regiman', 'idRegimenIncorporacionInt', 'idRegimenIncorporacionInt');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function asignacionAmbitos()
    {
        return $this->hasMany('App\Models\asignacion_ambito', 'idEnteLng', 'idEnteLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relacionLaborals()
    {
        return $this->hasMany('App\Models\relacion_laboral', 'idEnteLng', 'idEnteLng');
    }
}
