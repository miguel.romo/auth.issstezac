<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idAsignacionSucursalLng
 * @property int $idUsuarioLng
 * @property int $idSucursarlLng
 * @property Sucursal[] $sucursals
 */
class asignacion_sucursales extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idAsignacionSucursalLng';

    /**
     * @var array
     */
    protected $fillable = ['idUsuarioLng', 'idSucursarlLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sucursals()
    {
        return $this->hasMany('App\Models\sucursal', 'asignacion_sucursales_idAsignacionSucursalLng', 'idAsignacionSucursalLng');
    }
}
