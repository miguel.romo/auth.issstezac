<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombreRol
 * @property string $descripcion
 */
class UserRol extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user_rol';

    /**
     * @var array
     */
    protected $fillable = ['nombreRol', 'descripcion'];
    public $timestamps = false;
}
