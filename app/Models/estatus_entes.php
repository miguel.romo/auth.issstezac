<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idEstatusEnteInt
 * @property string $nombreEstatusEnte
 * @property EntePublico[] $entePublicos
 */
class estatus_entes extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idEstatusEnteInt';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreEstatusEnte'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'idEstatusEnteInt', 'idEstatusEnteInt');
    }
}
