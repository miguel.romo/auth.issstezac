<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $nombre
 * @property string $rfcPatronal
 * @property string $telefono
 * @property string $clasificacion
 * @property string $fechaIncorporacion
 * @property string $fechaDesincorporacion
 * @property float $porcentajeCuota
 * @property float $porcentajeAportacion
 * @property string $claveRegistroPatronal
 * @property int $idRegimenIncorporacion
 * @property int $idEstatusEnte
 * @property int $idRepresentanteLegal
 * @property int $idTabulador
 * @property integer $idDomicilio
 * @property int $idCalendarioCobros
 * @property int $idConceptoEnte
 */
class EntePublico extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'ente_publico';

    /**
     * @var array
     */
    protected $fillable = ['nombre', 'rfcPatronal', 'telefono', 'clasificacion', 'fechaIncorporacion', 'fechaDesincorporacion', 'porcentajeCuota', 'porcentajeAportacion', 'claveRegistroPatronal', 'idRegimenIncorporacion', 'idEnteEstatus', 'idRepresentanteLegal', 'idTabulador', 'idDomicilio', 'idCalendarioCobros', 'idEnteConcepto'];
    
    public $timestamps = false;
}
