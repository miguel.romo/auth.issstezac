<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $idParametrosSistemaLng
 * @property int $estatusInt
 * @property string $observacionesStr
 * @property int $tipoCierreInt
 * @property string $horaCierreDte
 * @property float $salarioMinimoBid
 * @property float $inflacionDbl
 * @property float $TIIEDbl
 * @property int $tiempoExpiraChequeInt
 * @property float $pLiquidezDerechohabienteInt
 * @property int $plazoDevolucionCuotasInt
 * @property float $pInteresEntregaPersonalDbl
 * @property int $plazoReintegrarIndemnizacionInt
 */
class parametros_sistema extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'parametros_sistema';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idParametrosSistemaLng';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['estatusInt', 'observacionesStr', 'tipoCierreInt', 'horaCierreDte', 'salarioMinimoBid', 'inflacionDbl', 'TIIEDbl', 'tiempoExpiraChequeInt', 'pLiquidezDerechohabienteInt', 'plazoDevolucionCuotasInt', 'pInteresEntregaPersonalDbl', 'plazoReintegrarIndemnizacionInt'];

}
