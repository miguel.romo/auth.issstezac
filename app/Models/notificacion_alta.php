<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idNotificacionAltaLng
 * @property string $estatusStr
 * @property string $rfcStr
 * @property string $curpStr
 * @property string $nssStr
 * @property string $nombreStr
 * @property string $primerApellidoStr
 * @property string $segundoApellidoStr
 * @property string $sexoStr
 * @property string $fechaNacDte
 * @property string $fechaDefuncion
 * @property string $fechaIngreso
 * @property string $estatusBloqueoStr
 * @property string $telefono
 * @property string $celularStr
 * @property string $mail
 * @property string $mailAlterno
 * @property string $numeroIssstezacStr
 * @property int $idEstadoCivilLng
 * @property int $idTipoDerechohabienteLng
 * @property int $idGrupoSanguineoLng
 * @property integer $domicilio_idDomicilioLng
 * @property int $beneficiarios_idBeneficiarioLng
 * @property string $TIPOVIAL
 * @property string $NOMVIAL
 * @property string $CARRETERA
 * @property string $CAMINO
 * @property int $NUMEXTNUM
 * @property string $NUMEXTALF
 * @property string $NUMEXTANT
 * @property int $NUMINTNUM
 * @property string $NUMINTALF
 * @property string $TIPOASEN
 * @property string $NOMASEN
 * @property string $CP
 * @property string $NOM_LOC
 * @property string $CVE_LOC
 * @property string $NOM_MUN
 * @property string $CVE_MUN
 * @property string $NOM_ENT
 * @property string $CVE_ENT
 * @property string $TIPOREF1
 * @property string $NOMREF1
 * @property string $TIPOREF2
 * @property string $NOMREF2
 * @property string $TIPOREF3
 * @property string $NOMREF3
 * @property string $DESCRUBIC
 * @property int $idTipoDomicilioLng
 * @property int $idRelacionLaboralLng
 * @property string $estatusStr_copy1
 * @property string $numeroEmpleadoStr
 * @property string $fechaIngresoDte
 * @property string $fechaBajaDefinitivaDte
 * @property string $puestoStr
 * @property int $credencialVigenteBit
 * @property string $credencialStr
 * @property int $periodicidadInt
 * @property float $sueldoQuincenalDbl
 * @property float $sueldoBaseCotizacionDbl
 * @property float $liquidezDbl
 * @property float $liquidezApartadaDbl
 * @property string $clabeStr
 * @property string $cuentaBancariaStr
 * @property int $idDerechohabienteLng
 * @property int $idSectorLng
 * @property int $idEnteLng
 * @property int $idMunicipioLng
 * @property int $idTipoRegimenLng
 * @property int $idLeyLng
 * @property int $idSindicatoLng
 * @property int $idBancoLng
 * @property int $idCentroCostosLng
 * @property int $idTabuladorLng
 * @property int $idNotifiacionLng
 * @property Notificacion[] $notificacions
 * @property Tabulador[] $tabuladors
 */
class notificacion_alta extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notificacion_alta';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idNotificacionAltaLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['estatusStr', 'rfcStr', 'curpStr', 'nssStr', 'nombreStr', 'primerApellidoStr', 'segundoApellidoStr', 'sexoStr', 'fechaNacDte', 'fechaDefuncion', 'fechaIngreso', 'estatusBloqueoStr', 'telefono', 'celularStr', 'mail', 'mailAlterno', 'numeroIssstezacStr', 'idEstadoCivilLng', 'idTipoDerechohabienteLng', 'idGrupoSanguineoLng', 'domicilio_idDomicilioLng', 'beneficiarios_idBeneficiarioLng', 'TIPOVIAL', 'NOMVIAL', 'CARRETERA', 'CAMINO', 'NUMEXTNUM', 'NUMEXTALF', 'NUMEXTANT', 'NUMINTNUM', 'NUMINTALF', 'TIPOASEN', 'NOMASEN', 'CP', 'NOM_LOC', 'CVE_LOC', 'NOM_MUN', 'CVE_MUN', 'NOM_ENT', 'CVE_ENT', 'TIPOREF1', 'NOMREF1', 'TIPOREF2', 'NOMREF2', 'TIPOREF3', 'NOMREF3', 'DESCRUBIC', 'idTipoDomicilioLng', 'idRelacionLaboralLng', 'estatusStr_copy1', 'numeroEmpleadoStr', 'fechaIngresoDte', 'fechaBajaDefinitivaDte', 'puestoStr', 'credencialVigenteBit', 'credencialStr', 'periodicidadInt', 'sueldoQuincenalDbl', 'sueldoBaseCotizacionDbl', 'liquidezDbl', 'liquidezApartadaDbl', 'clabeStr', 'cuentaBancariaStr', 'idDerechohabienteLng', 'idSectorLng', 'idEnteLng', 'idMunicipioLng', 'idTipoRegimenLng', 'idLeyLng', 'idSindicatoLng', 'idBancoLng', 'idCentroCostosLng', 'idTabuladorLng', 'idNotifiacionLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notificacions()
    {
        return $this->hasMany('App\Models\notificacion', 'notificacion_alta_idNotificacionAltaLng', 'idNotificacionAltaLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tabuladors()
    {
        return $this->hasMany('App\Models\tabulador', 'notificacion_alta_idNotificacionAltaLng', 'idNotificacionAltaLng');
    }
}
