<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idNotificacionModificacion
 * @property float $sueldoAnterorDbl
 * @property float $nuevoSueldoDbl
 * @property int $idMotivoModificacion
 * @property string $categoriaAnteriorStr
 * @property string $nuevaCategoriaStr
 * @property int $idTabuladorng
 * @property int $notificacion_idNotificacionLng
 * @property Notificacion $notificacion
 * @property MotivoModificacion[] $motivoModificacions
 * @property Tabulador[] $tabuladors
 */
class notificacion_modificacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'notificacion_modificacion';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idNotificacionModificacion';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['sueldoAnterorDbl', 'nuevoSueldoDbl', 'idMotivoModificacion', 'categoriaAnteriorStr', 'nuevaCategoriaStr', 'idTabuladorng', 'notificacion_idNotificacionLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notificacion()
    {
        return $this->belongsTo('App\Models\Notificacion', 'notificacion_idNotificacionLng', 'idNotificacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function motivoModificacions()
    {
        return $this->hasMany('App\Models\motivo_modificacion', 'notificacion_modificacion_idNotificacionModificacion', 'idNotificacionModificacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tabuladors()
    {
        return $this->hasMany('App\Models\tabulador', 'notificacion_modificacion_idNotificacionModificacion', 'idNotificacionModificacion');
    }
}
