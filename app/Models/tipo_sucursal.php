<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $idTipoSucursalLng
 * @property string $nombreTipoSucursalStr
 * @property integer $c_sucursal_idUnidadFilialLng
 * @property Sucursal $sucursal
 */
class tipo_sucursal extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'tipo_sucursal';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idTipoSucursalLng';

    /**
     * The "type" of the auto-incrementing ID.
     * 
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreTipoSucursalStr', 'c_sucursal_idUnidadFilialLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sucursal()
    {
        return $this->belongsTo('App\Models\sucursal', 'c_sucursal_idUnidadFilialLng', 'idUnidadFilialLng');
    }
}
