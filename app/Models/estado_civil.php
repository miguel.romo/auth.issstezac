<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idEstadoCivilLng
 * @property string $nombreEstadoCivilStr
 * @property Beneficiario[] $beneficiarios
 * @property Derechohabiente[] $derechohabientes
 */
class estado_civil extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'estado_civil';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idEstadoCivilLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['nombreEstadoCivilStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function beneficiarios()
    {
        return $this->hasMany('App\Models\beneficiarios', 'estado_civil_idEstadoCivilLng', 'idEstadoCivilLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function derechohabientes()
    {
        return $this->hasMany('App\Models\derechohabiente', 'idEstadoCivilLng', 'idEstadoCivilLng');
    }
}
