<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idMovimientoCobroLng
 * @property int $idPropuestaCobroLng
 * @property int $idConceptoCobroLng
 * @property string $estatusMovimientoStr
 * @property string $autodeterminacionStr
 * @property float $montoMovimiento
 * @property int $idRelacionLaboralLng
 * @property int $idPagoAdeudo
 * @property int $nota_autodeterminacion_idNotaAutodeterminacionLng
 * @property int $idConceptoEnteLng
 * @property NotaAutodeterminacion $notaAutodeterminacion
 * @property ConceptoCobro[] $conceptoCobros
 * @property PropuestaCobro[] $propuestaCobros
 * @property RelacionLaboral[] $relacionLaborals
 */
class movimientos_cobros extends Model
{
    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idMovimientoCobroLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idPropuestaCobroLng', 'idConceptoCobroLng', 'estatusMovimientoStr', 'autodeterminacionStr', 'montoMovimiento', 'idRelacionLaboralLng', 'idPagoAdeudo', 'nota_autodeterminacion_idNotaAutodeterminacionLng', 'idConceptoEnteLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notaAutodeterminacion()
    {
        return $this->belongsTo('App\Models\nota_autodeterminacion', 'nota_autodeterminacion_idNotaAutodeterminacionLng', 'idNotaAutodeterminacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function conceptoCobros()
    {
        return $this->hasMany('App\Models\concepto_cobro', 'movimientos_cobros_idMovimientoCobroLng', 'idMovimientoCobroLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propuestaCobros()
    {
        return $this->hasMany('App\Models\propuesta_cobro', 'movimientos_cobros_idMovimientoCobroLng', 'idMovimientoCobroLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relacionLaborals()
    {
        return $this->hasMany('App\Models\relacion_laboral', 'movimientos_cobros_idMovimientoCobroLng', 'idMovimientoCobroLng');
    }
}
