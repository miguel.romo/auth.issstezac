<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idPeriodoCotizacionLng
 * @property int $idRelacionLaboralLng
 * @property string $estatusPeriodoStr
 * @property float $sueldoCotizacion
 * @property string $fechaInicioDte
 * @property string $fechaFinDte
 * @property int $idTipoBaja
 * @property int $idMotivoBaja
 * @property MotivoBaja[] $motivoBajas
 * @property RelacionLaboral[] $relacionLaborals
 * @property TipoBaja[] $tipoBajas
 */
class periodo_cotizacion extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'periodo_cotizacion';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idPeriodoCotizacionLng';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['idRelacionLaboralLng', 'estatusPeriodoStr', 'sueldoCotizacion', 'fechaInicioDte', 'fechaFinDte', 'idTipoBaja', 'idMotivoBaja'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function motivoBajas()
    {
        return $this->hasMany('App\Models\motivo_baja', 'periodo_cotizacion_idPeriodoCotizacionLng', 'idPeriodoCotizacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relacionLaborals()
    {
        return $this->hasMany('App\Models\relacion_laboral', 'periodo_cotizacion_idPeriodoCotizacionLng', 'idPeriodoCotizacionLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipoBajas()
    {
        return $this->hasMany('App\Models\tipo_baja', 'periodo_cotizacion_idPeriodoCotizacionLng', 'idPeriodoCotizacionLng');
    }
}
