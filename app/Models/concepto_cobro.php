<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idConceptoCobroId
 * @property string $conceptoCobroStr
 * @property int $concepto_ente_idConceptoEnteLng
 * @property int $movimientos_cobros_idMovimientoCobroLng
 * @property ConceptoEnte $conceptoEnte
 * @property MovimientosCobro $movimientosCobro
 */
class concepto_cobro extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'concepto_cobro';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idConceptoCobroId';

    /**
     * Indicates if the IDs are auto-incrementing.
     * 
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var array
     */
    protected $fillable = ['conceptoCobroStr', 'concepto_ente_idConceptoEnteLng', 'movimientos_cobros_idMovimientoCobroLng'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function conceptoEnte()
    {
        return $this->belongsTo('App\Models\concepto_ente', 'concepto_ente_idConceptoEnteLng', 'idConceptoEnteLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function movimientosCobro()
    {
        return $this->belongsTo('App\Models\movimientos_cobros', 'movimientos_cobros_idMovimientoCobroLng', 'idMovimientoCobroLng');
    }
}
