<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idDocumentoLng
 * @property int $idRelacionadoLng
 * @property int $idTipoDocumentoLng
 * @property string $ubicacionStr
 * @property int $nota_autodeterminacion_idNotaAutodeterminacionLng
 * @property Derechohabiente $derechohabiente
 * @property EntePublico[] $entePublicos
 * @property RepresentanteLegal $representanteLegal
 * @property TipoDocumento[] $tipoDocumentos
 */
class documento_digitalizado extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'documento_digitalizado';

    /**
     * @var array
     */
    protected $fillable = ['ubicacionStr'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function derechohabiente()
    {
        return $this->hasOne('App\Models\derechohabiente', 'idDerechohabienteLng', 'idRelacionadoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function entePublicos()
    {
        return $this->hasMany('App\Models\ente_publico', 'idEnteLng', 'idRelacionadoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function representanteLegal()
    {
        return $this->hasOne('App\Models\representante_legal', 'idRepresentanteLegal', 'idRelacionadoLng');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipoDocumentos()
    {
        return $this->hasMany('App\Models\tipo_documento', 'documento_digitalizado_idDocumentoLng', 'idDocumentoLng');
    }
}
