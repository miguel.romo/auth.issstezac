<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $idSectorLng
 * @property string $nombreSectorStr
 * @property string $letraIdentificadoraStr
 * @property float $porcentajeAportacionDbl
 * @property float $porcentajeCuotaDbl
 * @property RelacionLaboral[] $relacionLaborals
 */
class sector extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'sector';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'idSectorLng';

    /**
     * @var array
     */
    protected $fillable = ['nombreSectorStr', 'letraIdentificadoraStr', 'porcentajeAportacionDbl', 'porcentajeCuotaDbl'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function relacionLaborals()
    {
        return $this->hasMany('App\Models\relacion_laboral', 'idSectorLng', 'idSectorLng');
    }
}
