<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $idVialidad
 * @property string $nombreVialidad
 * @property string $numExt
 * @property string $numInt
 * @property int $idAsentamiento
 * @property string $referencia
 * @property int $idTipoDomicilio
 */
class Domicilio extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'domicilio';

    /**
     * @var array
     */
    protected $fillable = ['idVialidad', 
                            'nombreVialidad',
                            'numero', 
                            'idAsentamiento', 
                            'referencia', 
                            'idTipoDomicilio'
                        ];
    public $timestamps = false;              

}
