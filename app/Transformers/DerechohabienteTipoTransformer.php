<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\DerechohabienteTipo;
use League\Fractal\TransformerAbstract;

class DerechohabienteTipoTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(DerechohabienteTipo $tipo)
    {

        return [
                'id'       => (int) $tipo->id,
                'nombre'   => $tipo->descripcion, 
                'inicial'   => $tipo->inicial,   
        ];
    }
}