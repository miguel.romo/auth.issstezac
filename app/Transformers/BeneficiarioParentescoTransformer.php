<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\BeneficiarioParentesco;
use League\Fractal\TransformerAbstract;

class BeneficiarioParentescoTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(BeneficiarioParentesco $parentesco)
    {

        return [
                'id'        => (int) $parentesco->id,
                'parentesco'   => $parentesco->parentesco,  
        ];
    }
}