<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserTransformer;
use App\Models\Usuario;
use League\Fractal\TransformerAbstract;

class UsersTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Usuario $user)
    {

        return [
            'id'        => (int) $user->id,
            'username'      => $user->username,
            'email'     => $user->email,
            'nombre'    => $user->nombre,
            'primerApellido' => $user->primerApellido,
            'segundoApellido' => $user->segundoApellido,
            'iniciales' => $user->iniciales,  
            'idRol' => $user->idRol,
        ];
    }
}