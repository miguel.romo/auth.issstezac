<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\EntidadFederativa;
use League\Fractal\TransformerAbstract;

class EntidadFederativaTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(EntidadFederativa $entidad)
    {

        return [
                'id'       => (int) $entidad->id,
                'nombre'   => $entidad->nombre, 
                'cveEnt'   => $entidad->cveEnt, 
                'abreviatura'  => $entidad->abreviatura,  
        ];
    }
}