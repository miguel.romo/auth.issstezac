<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Derechohabiente;
use League\Fractal\TransformerAbstract;

class DerechohabienteTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Derechohabiente $derechohabiente)
    {

        return [
                'id'        => (int) $derechohabiente->id,
                'estatus'   => $derechohabiente->estatus, 
                'rfc'       => $derechohabiente->rfc, 
                'curp'      => $derechohabiente->curp, 
                'nss'       => $derechohabiente->nss, 
                'nombre'    => $derechohabiente->nombre, 
                'primerApellido'  => $derechohabiente->primerApellido, 
                'segundoApellido' => $derechohabiente->segundoApellido, 
                'sexo'      => $derechohabiente->sexo, 
                'fechaNac'      => $derechohabiente->fechaNac, 
                'fechaDefuncion'=> $derechohabiente->fechaDefuncion, 
                'fechaIngreso'  => $derechohabiente->fechaIngreso, 
                'estatusBloqueo' => $derechohabiente->estatusBloqueo, 
                'telefono'      => $derechohabiente->telefono, 
                'celular'       => $derechohabiente->celular, 
                'email'         => $derechohabiente->email, 
                'emailAlterno'  => $derechohabiente->emailAlterno, 
                'numeroIssstezac' => $derechohabiente->numeroIssstezac, 
                'idEstadoCivil' => $derechohabiente->idEstadoCivil,
                'idDerechohabienteTipo' => $derechohabiente->idDerechohabienteTipo, 
                'idGrupoSanguineo'  => $derechohabiente->idGrupoSanguineo,   
                'idDomicilio' => $derechohabiente->idDomicilio,
                'idBeneficiario' => $derechohabiente->idBeneficiario, 
                'idNotificacion'  => $derechohabiente->idNotificacion,   
        ];
    }
}