<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\EstadoCivil;
use League\Fractal\TransformerAbstract;

class EstadoCivilTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(EstadoCivil $estado)
    {

        return [
                'id'        => (int) $estado->id,
                'nombreEstado'   => $estado->nombreEstado,  
        ];
    }
}