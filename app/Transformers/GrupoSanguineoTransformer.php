<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\GrupoSanguineo;
use League\Fractal\TransformerAbstract;

class GrupoSanguineoTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(GrupoSanguineo $grupo)
    {

        return [
                'id'        => (int) $grupo->id,
                'grupoSanguineo'   => $grupo->grupoSanguineo,  
        ];
    }
}