<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserRolTransformer;
use App\Models\UserRol;
use League\Fractal\TransformerAbstract;

class UserRolTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(UserRol $userRol)
    {

        return [
            'id'        => (int) $userRol->id,
            'nombreRol'      => $userRol->nombreRol,
            'descripcion'     => $userRol->descripcion,
        ];
    }
}