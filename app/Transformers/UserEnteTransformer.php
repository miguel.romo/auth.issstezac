<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Transformers\UserEnteTransformer;
use App\Models\UserEnte;
use League\Fractal\TransformerAbstract;

class UserEnteTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(UserEnte $userEnte)
    {

        return [
            'id'        => (int) $userEnte->id,
            'idUsuario'      => $userEnte->idUsuario,
            'idEnte'     => $userEnte->idEnte,
        ];
    }
}