<?php

namespace App\Transformers;
use Dingo\Api\Routing\Helpers;
use App\Models\Domicilio;
use League\Fractal\TransformerAbstract;

class DomicilioTransformer extends TransformerAbstract
{ 
    use Helpers;

    public function transform(Domicilio $domicilio)
    {

        return [
                'id'        => (int) $domicilio->id,
                'idVialidad'     =>  $domicilio->idVialidad, 
                'nombreVialidad' =>  $domicilio->nombreVialidad,
                'numero'         =>  $domicilio->numExt,
                'idAsentamiento' =>  $domicilio->idAsentamiento, 
                'referencia'     =>  $domicilio->referencia,    
                'idTipoDomicilio' =>  $domicilio->idTipoDomicilio       
        ];
    }
}